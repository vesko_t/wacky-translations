package com.vesodev.wackytranslatorf103131.util

import com.vesodev.wackytranslatorf103131.model.Contents
import com.vesodev.wackytranslatorf103131.model.WackyResponse
import com.vesodev.wackytranslatorf103131.model.enums.Translations
import org.json.JSONObject
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class HttpUtils {
    companion object {
        fun translateText(text: String, translation: Translations): WackyResponse {
            val url = "https://api.funtranslations.com/translate/${translation.value}.json?text=$text"
            return sendGetRequest(url)
        }

        private fun sendGetRequest(url: String): WackyResponse {
            val result = StringBuilder()
            val urlConnection = URL(url).openConnection() as HttpURLConnection
            val inputStream = urlConnection.inputStream
            val isr = InputStreamReader(inputStream)
            isr.forEachLine {
                result.append(it)
            }
            return mapObject(result.toString())
        }

        private fun mapObject(string: String): WackyResponse {
            val jsonObject = JSONObject(string)
            val contents: JSONObject = jsonObject.getJSONObject("contents")
                ?: throw TooManyRequestsException(jsonObject.getJSONObject("error").getString("message"))
            return WackyResponse(
                contents = Contents(
                    translated = contents.getString("translated"),
                    text = contents.getString("text"),
                    translation = contents.getString("translation")
                )
            )
        }
    }
}