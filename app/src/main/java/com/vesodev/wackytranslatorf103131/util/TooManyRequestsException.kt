package com.vesodev.wackytranslatorf103131.util

class TooManyRequestsException(override val message: String) : RuntimeException(message)