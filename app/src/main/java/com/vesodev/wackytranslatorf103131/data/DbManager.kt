package com.vesodev.wackytranslatorf103131.data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import com.vesodev.wackytranslatorf103131.model.Translation

class DbManager(private val context: Context) {
    private lateinit var dbHelper: DatabaseHelper
    private lateinit var database: SQLiteDatabase

    @Throws(SQLException::class)
    fun open(): DbManager {
        dbHelper = DatabaseHelper(context)
        database = dbHelper.writableDatabase
        return this
    }

    fun close() {
        dbHelper.close()
    }

    fun insert(translation: Translation) {
        val contentValue = ContentValues()
        contentValue.put(DatabaseHelper.ID, translation.id)
        contentValue.put(DatabaseHelper.TRANSLATED_TEXT, translation.translatedText)
        contentValue.put(DatabaseHelper.ORIGINAL_SENTENCE, translation.originalText)
        contentValue.put(DatabaseHelper.LANGUAGE, translation.language)
        database.insert(DatabaseHelper.TABLE_NAME, null, contentValue)
    }

    fun findById(id: Int): Translation? {
        val cursor: Cursor = database.query(
            DatabaseHelper.TABLE_NAME,
            arrayOf("id", "translated_text", "original_sentence", "language"),
            "id = ?",
            arrayOf(id.toString()),
            null,
            null,
            null
        )
        if (!cursor.moveToFirst()) {
            return null
        }
        return Translation(
            cursor.getInt(0),
            cursor.getString(1),
            cursor.getString(2),
            cursor.getString(3)
        )
    }

    fun findAll(): List<Translation> {
        val cursor: Cursor = database.query(
            DatabaseHelper.TABLE_NAME,
            arrayOf("id", "translated_text", "original_sentence", "language"),
            null,
            null,
            null,
            null,
            null
        )
        if (!cursor.moveToFirst()) {
            return ArrayList<Translation>()
        }
        val translations: MutableList<Translation> = ArrayList<Translation>()
        do {
            val advice = Translation(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3)
            )
            translations.add(advice)
        } while (cursor.moveToNext())
        return translations
    }

    fun delete(id: Int) {
        database.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.ID + "=" + id, null)
    }
}