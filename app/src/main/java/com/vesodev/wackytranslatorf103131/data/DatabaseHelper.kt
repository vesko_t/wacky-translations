package com.vesodev.wackytranslatorf103131.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(context: Context?) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    companion object {
        const val TABLE_NAME = "TRANSLATIONS"

        const val ID = "id"
        const val TRANSLATED_TEXT = "translated_text"
        const val ORIGINAL_SENTENCE = "original_sentence"
        const val LANGUAGE = "language"

        const val DB_NAME = "WackyTranslations.DB"

        // database version
        const val DB_VERSION = 1

        // Creating table query
        private const val CREATE_TABLE = ("create table $TABLE_NAME (ID INTEGER PRIMARY KEY, $TRANSLATED_TEXT TEXT NOT NULL, $ORIGINAL_SENTENCE TEXT NOT NULL, $LANGUAGE TEXT NOT NULL);")
    }
}