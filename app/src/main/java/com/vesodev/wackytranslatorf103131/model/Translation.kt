package com.vesodev.wackytranslatorf103131.model


data class Translation(val id: Int? = null, val translatedText: String, val originalText: String, val language: String)
