package com.vesodev.wackytranslatorf103131.model

data class WackyResponse(val contents: Contents)

data class Contents(val translated: String, val text: String, val translation: String)
