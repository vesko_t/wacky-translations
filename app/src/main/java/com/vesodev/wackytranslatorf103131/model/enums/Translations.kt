package com.vesodev.wackytranslatorf103131.model.enums

enum class Translations(val value: String) {
    YODA("yoda"),
    PIRATE("pirate"),
    VALYRIAN("valyrian"),
    MINION("minion"),
    SHAKESPEARE("shakespeare"),
    SITH("sith"),
    GUNGAN("gungan"),
    MANDALORIAN("mandalorian"),
    DOTHRAKI("dothraki"),
    GROOT("groot")
}
