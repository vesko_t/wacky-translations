package com.vesodev.wackytranslatorf103131.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.vesodev.wackytranslatorf103131.databinding.FragmentHomeBinding
import com.vesodev.wackytranslatorf103131.model.WackyResponse
import com.vesodev.wackytranslatorf103131.model.enums.Translations
import com.vesodev.wackytranslatorf103131.util.HttpUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.launch
import com.vesodev.wackytranslatorf103131.data.DbManager
import com.vesodev.wackytranslatorf103131.model.Translation
import com.vesodev.wackytranslatorf103131.util.TooManyRequestsException
import java.io.FileNotFoundException

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private lateinit var dbManager: DbManager

    private var currentTranslation: Translation? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val spinner: Spinner = binding.spinner

        val items = enumValues<Translations>().sortedBy { it.value }

        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, items)

        val translateButton = binding.button

        val inputText = binding.editTextTextMultiLine

        val textView = binding.textView

        val fabButton = binding.fabButton

        lifecycleScope.launch {
            dbManager = DbManager(requireContext())
        }

        translateButton.setOnClickListener {
            if (inputText.text.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Please enter text to translate",
                    Toast.LENGTH_SHORT
                ).show()
                println("Please enter text to translate")
                return@setOnClickListener
            }
            if (spinner.selectedItem == null) {
                Toast.makeText(requireContext(), "Please select a language", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            val selectedTranslation = spinner.selectedItem as Translations
            lifecycleScope.launch {
                try {
                    val translatedText: WackyResponse = withContext(Dispatchers.IO) {
                        HttpUtils.translateText(inputText.text.toString(), selectedTranslation)
                    }
                    textView.text = translatedText.contents.translated
                    currentTranslation = Translation(
                        translatedText = translatedText.contents.translated,
                        originalText = inputText.text.toString(),
                        language = selectedTranslation.value
                    )
                } catch (e: TooManyRequestsException) {
                    Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG)
                        .show()
                } catch (e: FileNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        "An error has occurred try again later.",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            }
        }

        fabButton.setOnClickListener {
            lifecycleScope.launch {
                if (currentTranslation == null) {
                    Toast.makeText(
                        requireContext(),
                        "Please translate text before saving",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@launch
                }
                dbManager.open()
                dbManager.insert(currentTranslation!!)
                dbManager.close()
                Toast.makeText(requireContext(), "Translation saved", Toast.LENGTH_SHORT).show()
            }
        }

        spinner.adapter = adapter
        spinner.setSelection(0)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}