package com.vesodev.wackytranslatorf103131.ui.savedTranslations

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.vesodev.wackytranslatorf103131.R
import com.vesodev.wackytranslatorf103131.data.DbManager
import com.vesodev.wackytranslatorf103131.model.Translation

class ListItemAdapter(
    private val context: Context,
    private val data: List<Translation>,
    private val dbManager: DbManager
) : ArrayAdapter<Translation>(context, 0, data) {

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Translation {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view =
            convertView ?: LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)

        val item = getItem(position)

        val titleTextView = view.findViewById<TextView>(R.id.titleTextView)
        val subtitleTextView = view.findViewById<TextView>(R.id.subtitleTextView)
        val smallTextView = view.findViewById<TextView>(R.id.smallTextView)
        val deleteButton = view.findViewById<TextView>(R.id.button)

        titleTextView.text = item.translatedText
        subtitleTextView.text = item.originalText
        smallTextView.text = item.language

        deleteButton.setOnClickListener {
            dbManager.open()
            item.id?.let { dbManager.delete(it) }
            (data as ArrayList).removeAt(position)
            notifyDataSetChanged()
            dbManager.close()
            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show()
        }

        return view
    }
}
