package com.vesodev.wackytranslatorf103131.ui.savedTranslations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.vesodev.wackytranslatorf103131.data.DbManager
import com.vesodev.wackytranslatorf103131.databinding.FragmentNotificationsBinding
import kotlinx.coroutines.launch

class SavedTranslationsFragment : Fragment() {

    private var _binding: FragmentNotificationsBinding? = null

    private lateinit var dbManager: DbManager

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val translations = binding.translations

        lifecycleScope.launch {
            dbManager = DbManager(requireContext())
            dbManager.open()
            translations.adapter = ListItemAdapter(requireContext(), dbManager.findAll(), dbManager)
            dbManager.close()
        }


        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}